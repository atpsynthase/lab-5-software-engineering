#include<iostream>
#include<lab5.h>
using namespace std;

CArray::CArray(){
    arraySize = 0; arrayName = nullptr;
}

CArray::CArray(int n){
    arraySize = n;
    arrayName = new int [arraySize];
    for (int i = 0; i < arraySize; i++){
        cout << "\nEnter value " << i+1 << ": ";
        cin >> *(arrayName+i);}
    cout << "\nAll compnents of the array initialized.\n";
}

CArray::~CArray(){
    if (arrayName) delete [] arrayName;
    cout << "\nArray Destroyed.\n";
}

void CArray::Swap(int* a, int* b){
    int temp; temp = *a; *a = *b; *b = temp;
}

void CArray::bubbleSort(){
    for (int i = 0; i < arraySize - 1; i++){
        int k=0;
        for (int j = 0; j < arraySize - i - 1; j++){
            if (*(arrayName+j) > *(arrayName+j+1)){
                CArray::Swap((arrayName+j), arrayName+j+1);
                k=1;}
//            cout << "\nInteration " << i << ": \t";
//            CArray::printArray();
        }
        if(k==0)
            break;
        cout << "\nAfter " << i << " interation: ";
        CArray::printArray();}}

void CArray::printArray(){
    for (int i = 0; i < arraySize; i++)
        cout << *(arrayName+i) << "\t";
}

TreeNode::TreeNode(){
    key = 0;
    left = right = nullptr;
}

TreeNode::TreeNode (CArray array){
    for(int i=0 ; i < array.arraySize ; i++)
    {
        if(i==0)
        {
            this->key = array.arrayName[i];
        }
        else
        {
            this->AddLeaf(array.arrayName[i]);
        }
    }
}

TreeNode::TreeNode(int val){
    key = val;
    left = right = NULL;
}

void TreeNode::AddLeaf(int value)
{
    if( value >= this->key)
    {
        if( this->right == 0 )
        {
            this->right = new TreeNode;
            this->right->key=value;
        }
        else
        {
            this->right->AddLeaf(value);
        }
    }
    else
    {
        if( this->left == 0 )
        {
            this->left = new TreeNode;
            this->left->key=value;
        }
        else
        {
            this->left->AddLeaf(value);
        }
    }
}


void TreeNode::print(void)
{
    if( this->left != 0 )
    {
        this->left->print();
    }
    cout << this->key << " ";
    if( this->right != 0 )
    {
        this->right->print();
    }
}


/*

TreeNode *root = new TreeNode(array[0]){
    for (int i = 0; i < n; i++){
        TreeNode* e1 = array[i];
        root->add(e1);
    }


TreeNode
 *newNode(int item)
{
    struct Node *temp = new Node;
    temp->key = item;
    temp->left = temp->right = NULL;
    return temp;
}



void storeSorted(Node *root, int arr[], int &i)
{ if (root != NULL) {
        storeSorted(root->left, arr, i);
        arr[i++] = root->key;
        storeSorted(root->right, arr, i);}}

Node* insert(Node* node, int key) {
    if (node == NULL) return newNode(key);
    if (key < node->key)
        node->left = insert(node->left, key);
    else if (key > node->key)
        node->right = insert(node->right, key);
    return node; }


void treeSort(int arr[], int n){
    struct Node *root = NULL;
    root = insert(root, arr[0]);
    for (int i=1; i<n; i++)
        insert(root, arr[i]);

    // Store inoder traversal of the BST
    // in arr[]
    int i = 0;
    storeSorted(root, arr, i);
}
*/
