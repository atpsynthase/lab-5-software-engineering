#ifndef LAB5_H
#define LAB5_H

class CArray{
private:
public:
    int arraySize; int* arrayName;
    CArray();
    CArray(int);
    ~CArray();
    void bubbleSort(void);
    void Swap(int*, int*);
    void printArray(void);
    int getValue(int);
};

class TreeNode{
public:
    TreeNode();
    TreeNode(int);
    TreeNode (CArray);
    ~TreeNode();
    int key;
    TreeNode *left;
    TreeNode *right;
    void AddLeaf(int);
    void print();
};

#endif // LAB5_H
